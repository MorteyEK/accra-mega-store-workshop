package io.turntabl.accramegastore.services;

import io.turntabl.accramegastore.exception.UnavailableProductException;
import io.turntabl.accramegastore.services.product.Product;
import io.turntabl.accramegastore.services.product.ProductService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ShoppingCart {
    private final Map<String, Item> cartItems;
    private final ProductService productService;

    public ShoppingCart(ProductService productService) {
        this.productService = productService;
        this.cartItems = new HashMap<>();
    }

    public void addProduct(String productId, int quantity) {
        Product product = productService.getProduct(productId);

        if (product.getStockLevel() < quantity) {
            String message = format("product with id=%s is unavailable in the requested quantity", product.getId());
            throw new UnavailableProductException(message);
        }
        if (cartItems.containsKey(productId)) {
            Item item = cartItems.get(productId);
            item.setQuantity(item.getQuantity() + quantity);
        }
        else {
            cartItems.put(productId, new Item(productId, product.getName(), product.getBrand(),
                    product.getPrice(), quantity, product.getCategory()));
        }

        productService.updateProduct(productId, product.getStockLevel() - quantity);
    }

    public void removeProduct(String productId, int quantity) {
        Product product = productService.getProduct(productId);

        if (!cartItems.containsKey(productId)) {
            throw new UnavailableProductException("Basket does not contain item: " + productId);
        }

        Item item = cartItems.get(product.getId());
        int newQuantity = item.getQuantity() - quantity;

        if (newQuantity <= 0) {
            cartItems.remove(product.getId());
        } else {
            item.setQuantity(newQuantity);
        }
        productService.updateProduct(productId,
                product.getStockLevel() + Math.max(0, newQuantity));
    }

    public double calculateTotal() {
        int total = 0;
        for (Item item : cartItems.values()) {
            total += item.getPrice() * item.getQuantity();
        }
        return total;
    }

    public List<Item> getItems() {
        return new ArrayList<>(cartItems.values());
    }
}
