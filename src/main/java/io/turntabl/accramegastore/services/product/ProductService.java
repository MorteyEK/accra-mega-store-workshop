package io.turntabl.accramegastore.services.product;

import io.turntabl.accramegastore.exception.UnknownProductException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductService {
    private final List<Product> products;

    public ProductService(String filename) {
        products = loadProducts(filename);
    }

    private List<Product> loadProducts(String filename)  {
        List<Product> products = new ArrayList<>();
        List<String> lines;

        try {
            URL systemResource = ClassLoader.getSystemResource(filename);
            lines = Files.readAllLines(Paths.get(systemResource.toURI()));
            lines.remove(0);
        }
        catch (URISyntaxException | IOException e) {
            throw new RuntimeException("failed to load products");
        }

        for (String line : lines) {
            Optional<Product> p = parseProduct(line);
            p.ifPresent(products::add);
        }

        return products;
    }

    private Optional<Product> parseProduct(String line) {
        String[] values = line.split(",");

        String id = values[0].trim();
        String name = values[1].trim();
        String brand = values[2].trim();
        String category = values[5].trim();

        double sellingPrice;
        int quantity;

        try {
            sellingPrice = Double.parseDouble(values[3].trim());
        } catch (NumberFormatException e) {
            System.out.println("Invalid selling price, skipping product with id: " + values[3]);
            return Optional.empty();
        }

        try {
            quantity = Integer.parseInt(values[4].trim());
        } catch (NumberFormatException e) {
            System.out.println("Invalid quantity, skipping product with id: " + values[0]);
            return Optional.empty();
        }

        if (sellingPrice < 0 || quantity < 0) {
            System.out.println("Failed to load, skipping product with id: " + values[0]);
            return Optional.empty();
        }

        return Optional.of(new Product(id, name, brand, sellingPrice,
                quantity, category));
    }

    public List<Product> getProducts() {
        return products;
    }

    public Product getProduct(String productId) {
        for (Product product : products) {
            if (product.getId().equals(productId)) {
                return product;
            }
        }

        throw new UnknownProductException("Unknown Product: " + productId);
    }

    public void updateProduct(String productId, int stockLevel) {
        // verify greater than 0.
        for (Product product: products) {
            if (productId.equals(product.getId())) {
                product.setStockLevel(stockLevel);
                return;
            }
        }
        throw new UnknownProductException("Product unknown: " + productId);
    }
}
