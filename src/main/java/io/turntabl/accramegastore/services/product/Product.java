package io.turntabl.accramegastore.services.product;

import java.util.Objects;

public class Product {

    private final String id;
    private final String name;
    private final String brand;

    private final double price;
    private int quantity;

    private final String category;

    public Product(String ids, String name, String brand, double price,
                   int quantity, String category) {
        this.id = ids;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public int getStockLevel() {
        return quantity;
    }

    public void setStockLevel(int quantity) {
        this.quantity = quantity;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, brand, price, quantity, category);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;

        return Double.compare(product.price, price) == 0 &&
                quantity == product.quantity && id.equals(product.id) &&
                name.equals(product.name) && brand.equals(product.brand) &&
                category.equals(product.category);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", category='" + category + '\''+
                '}';
    }

    public String getBrand() {
        return brand;
    }
}
