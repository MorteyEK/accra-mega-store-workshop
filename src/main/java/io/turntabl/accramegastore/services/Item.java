package io.turntabl.accramegastore.services;

public class Item {
    private final String id;
    private final String name;
    private final String brand;
    private final double price;
    private int quantity;
    private final String category;

    public Item(String id, String name, String brand, double price, int quantity, String category) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setQuantity(int newQuantity) {
        this.quantity = newQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", category='" + category + '\'' +
                '}';
    }
}
