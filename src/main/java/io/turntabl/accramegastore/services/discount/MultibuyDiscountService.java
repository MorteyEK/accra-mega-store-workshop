package io.turntabl.accramegastore.services.discount;

import io.turntabl.accramegastore.services.Item;
import java.util.List;

public class MultibuyDiscountService implements DiscountService {


    public double calculateDiscount(List<Item> items) {
        double discount = 0d;

        for (Item item : items) {
            int itemsForFree = item.getQuantity() / 3;
            discount += itemsForFree * item.getPrice();
        }

        return discount;
    }
}
